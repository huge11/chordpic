import { useState } from 'react'
import { decompressFromEncodedURIComponent } from 'lz-string'

export function decompress<T>(compressedData: string): T | null {
  try {
    return JSON.parse(decompressFromEncodedURIComponent(compressedData))
  } catch (err) {
    // decompression failed -> return default value
    return null
  }
}

export function useCompressedState<T>(compressedData: string): [T | null, (value: T) => void] {
  return useState<T | null>(decompress<T>(compressedData))
}
