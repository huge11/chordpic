import * as React from 'react'
import { MutableRefObject } from 'react'
import { ImageService } from '../services/image-service'
import { Download } from 'react-feather'
import { IconButton } from './IconButton'

const downloadPng = (chartDom: HTMLDivElement | null, widht = 400, height = 600) => () => {
  if (!chartDom) {
    return
  }

  const svg = chartDom.innerHTML
  ImageService.downloadPng(svg, widht, height)
}

const downloadSvg = (chartDom: HTMLDivElement | null) => () => {
  if (!chartDom) {
    return
  }

  const svg = chartDom.innerHTML
  ImageService.downloadSvg(svg)
}

interface IProps {
  chartRef: MutableRefObject<HTMLDivElement | null>
  size: { height: number; width: number }
}

export const DownloadButtons = ({ chartRef, size }: IProps) => {
  // size multipliers (1 => original size)
  const pngSizeMultipliers: { multiplier: number; name: string }[] = [
    {
      multiplier: 0.5,
      name: 'Small'
    },
    {
      multiplier: 1,
      name: 'Medium'
    },
    {
      multiplier: 2,
      name: 'Large'
    },
    {
      multiplier: 4,
      name: 'Huge'
    }
  ]

  return (
    <>
      <h2>Download</h2>
      <IconButton size="sm" variant="outline-dark" onClick={downloadSvg(chartRef.current)}>
        <Download />
        SVG
      </IconButton>

      {pngSizeMultipliers.map(({ multiplier, name }, i) => {
        const width = Math.round(size.width * multiplier)
        const height = Math.round(size.height * multiplier)

        return (
          <IconButton size="sm" variant="outline-dark" key={i} onClick={downloadPng(chartRef.current, width, height)}>
            <Download />
            {name} PNG ({width} x {height})
          </IconButton>
        )
      })}
    </>
  )
}

export default DownloadButtons
