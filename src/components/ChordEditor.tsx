import * as React from 'react'
import { useEffect, useState } from 'react'
import TuningInput from './TuningInput'
import ChordInput from './ChordInput'
import SilentStringsInput from './SilentStringsInput'
import { ChordMatrix } from '../services/chord-matrix'
import { Chord, ChordSettings } from 'svguitar'
import Button from 'react-bootstrap/Button'
import { Trash2 } from 'react-feather'
import styled from '@emotion/styled'
import { Chart } from '../interfaces/chart'

const lineWidth = 3

export interface IChordInputSettings {
  width: number
  height: number
  lineWidth: number
  circleSize: number
}

interface IProps {
  numFrets: number
  numStrings: number
  chord: Chord
  settings: ChordSettings
  onChart: (newChart: Chart) => void
  width: number
  height: number
}

const ResetButton = styled(Button)`
  position: absolute;
  top: 0;
  right: 0;
  color: black;
`

function resize<T>(arr: T[], newSize: number, defaultValue: T) {
  if (newSize > arr.length) {
    return [...arr, ...Array(Math.max(newSize - arr.length, 0)).fill(defaultValue)]
  }

  return arr.slice(0, newSize)
}

export const ChordEditor = (props: IProps) => {
  const [matrix, setMatrix] = useState(
    ChordMatrix.fromChart({
      chord: props.chord,
      settings: props.settings
    })
  )

  useEffect(() => {
    matrix.setNumStrings(props.numStrings).setNumFrets(props.numFrets)
    setMatrix(matrix)

    // resize tuning array
    const tuning = resize(props.settings.tuning || [], props.numStrings, '')

    props.onChart({
      chord: matrix.toVexchord(),
      settings: {
        ...props.settings,
        tuning
      }
    })
  }, [props.numFrets, props.numStrings])

  const { settings, numStrings, width, height } = props

  const onMatrixChange = (newMatrix: ChordMatrix) => {
    // callback
    props.onChart({
      chord: newMatrix.toVexchord(),
      settings: props.settings
    })

    setMatrix(newMatrix)
  }

  const onTuningChange = (tuning: string[]) =>
    props.onChart({
      chord: props.chord,
      settings: {
        ...props.settings,
        tuning
      }
    })

  const onResetChord = () => {
    const newMatrix = new ChordMatrix(props.numFrets, props.numStrings)
    props.onChart({
      chord: newMatrix.toVexchord(),
      settings: {
        ...props.settings,
        tuning: Array(props.settings.strings).fill('')
      }
    })

    setMatrix(newMatrix)
  }

  const circleSize = Math.min(50, width / numStrings - 2)
  const displaySettings = { lineWidth, circleSize, width, height }

  return (
    <div>
      <ResetButton variant="link" onClick={onResetChord} title="Reset chord chart">
        <Trash2 />
      </ResetButton>
      <SilentStringsInput settings={displaySettings} matrix={matrix} onMatrixChange={onMatrixChange} />
      <ChordInput matrix={matrix} settings={displaySettings} onMatrixChange={onMatrixChange} />
      <TuningInput settings={displaySettings} tunings={settings.tuning || []} onTunings={onTuningChange} numStrings={numStrings} />
    </div>
  )
}
