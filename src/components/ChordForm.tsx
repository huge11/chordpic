import * as React from 'react'
import { ChangeEvent, useState } from 'react'
import constants from '../constants'
import { ChordSettings, ChordStyle } from 'svguitar'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import { FormControlProps } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import { ChevronDown, ChevronUp, Trash2 } from 'react-feather'
import styled from '@emotion/styled'
import { RangeInput } from './RangeInput'
import { ColorInput } from './ColorInput'
import { IconButton } from './IconButton'

interface IProps {
  settings: ChordSettings
  onSettings: (settings: ChordSettings) => void
  onResetSettings: () => void
}

const ButtonWithIcon = styled(Button)`
  padding-left: 0;

  svg {
    margin-right: 0.5rem;
    position: relative;
    top: -0.15rem;
  }
`

export const ChordForm = (props: IProps) => {
  const [showMoreSettings, setShowMoreSettings] = useState(false)

  const onNumericSettingChange = (
    settingName: string,
    constraints: {
      min?: number
      max?: number
    } = {}
  ) => (e: React.FormEvent<FormControlProps> | React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(e.currentTarget.value)

    // validate value
    if (
      isNaN(value) ||
      (typeof constraints.min !== 'undefined' && value < constraints.min) ||
      (typeof constraints.max !== 'undefined' && value > constraints.max)
    ) {
      return
    }

    props.onSettings({
      ...props.settings,
      [settingName]: value
    })
  }

  const onSettingsChange = (settingName: string) => (e: React.FormEvent<FormControlProps> | string) => {
    const value = typeof e === 'string' ? e : e.currentTarget.value

    props.onSettings({
      ...props.settings,
      [settingName]: value
    })
  }

  const onBooleanSettingChange = (settingName: string) => (e: ChangeEvent<HTMLInputElement>) => {
    props.onSettings({
      ...props.settings,
      [settingName]: e.target.checked
    })
  }

  const {
    settings: { frets, strings, title, position, style, fretSize, nutSize, strokeWidth, color, backgroundColor, fixedDiagramPosition }
  } = props

  return (
    <Form>
      <Form.Row>
        <Form.Group controlId="title" as={Col} lg="3" sm="6">
          <Form.Label column={true}>Title</Form.Label>
          <Form.Control type="text" placeholder="Title..." onChange={onSettingsChange('title')} value={title} />
        </Form.Group>
        <Form.Group controlId="startingFret" as={Col} lg="3" sm="6">
          <Form.Label column={true}>Starting Fret</Form.Label>
          <Form.Control
            type="number"
            placeholder="Starting fret..."
            onChange={onNumericSettingChange('position', { min: 1 })}
            value={String(position)}
          />
        </Form.Group>
        <Form.Group controlId="startingFret" as={Col} lg="3" sm="6">
          <Form.Label column={true}>Number of Frets</Form.Label>
          <Form.Control
            type="number"
            placeholder="Number of frets..."
            onChange={onNumericSettingChange('frets', { min: 2 })}
            max={constants.maxFrets}
            min="2"
            value={String(frets)}
          />
        </Form.Group>
        <Form.Group controlId="startingFret" as={Col} lg="3" sm="6">
          <Form.Label column={true}>Number of Strings</Form.Label>
          <Form.Control
            type="number"
            placeholder="Number of strings..."
            onChange={onNumericSettingChange('strings', { min: 2 })}
            max={constants.maxStrings}
            value={String(strings)}
          />
        </Form.Group>
      </Form.Row>

      <Form.Row>
        <Col>
          <ButtonWithIcon variant="link" onClick={() => setShowMoreSettings(!showMoreSettings)}>
            {showMoreSettings ? (
              <>
                <ChevronUp />
                Hide Settings
              </>
            ) : (
              <>
                <ChevronDown />
                Show More Settings
              </>
            )}
          </ButtonWithIcon>
        </Col>
      </Form.Row>

      {showMoreSettings && (
        <>
          <Form.Row>
            <Form.Group controlId="style" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Style</Form.Label>
              <Form.Control as="select" onChange={onSettingsChange('style')} value={style}>
                <option value={ChordStyle.normal}>Normal</option>
                <option value={ChordStyle.handdrawn}>Handdrawn</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="height" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Height</Form.Label>
              <RangeInput type="range" min={0.7} max={5} step={0.1} onChange={onNumericSettingChange('fretSize')} value={fretSize} />
            </Form.Group>

            <Form.Group controlId="nutSize" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Nut Size</Form.Label>
              <RangeInput type="range" min={0.3} max={1.5} step={0.025} onChange={onNumericSettingChange('nutSize')} value={nutSize} />
            </Form.Group>

            <Form.Group controlId="strokeWidth" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Stroke Width</Form.Label>
              <RangeInput type="range" min={1} max={10} step={0.25} onChange={onNumericSettingChange('strokeWidth')} value={strokeWidth} />
            </Form.Group>
          </Form.Row>

          <Form.Row>
            <Form.Group controlId="color" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Color</Form.Label>
              <ColorInput onChange={onSettingsChange('color')} value={color} />
            </Form.Group>

            <Form.Group controlId="backgroundColor" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Background Color</Form.Label>
              <ColorInput onChange={onSettingsChange('backgroundColor')} value={backgroundColor} />
            </Form.Group>

            <Form.Group controlId="fixedDiagramPosition" as={Col} lg="3" sm="6" className="text-lg-center">
              <Form.Label column={true}>Position</Form.Label>
              <Form.Check
                checked={!!fixedDiagramPosition}
                onChange={onBooleanSettingChange('fixedDiagramPosition')}
                custom
                type="checkbox"
                label="Fix Diagram Position"
              />
            </Form.Group>

            <Form.Group controlId="reset" as={Col} lg="3" sm="6">
              <Form.Label column={true}>Reset</Form.Label>
              <IconButton variant="outline-dark" onClick={props.onResetSettings}>
                <Trash2 />
                Reset all Settings
              </IconButton>
            </Form.Group>
          </Form.Row>
        </>
      )}
    </Form>
  )
}
