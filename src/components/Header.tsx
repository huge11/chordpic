import * as React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import logo from '../images/icon.png'
import { Link } from 'gatsby'
import styled from '@emotion/styled'

const NavbarToggle = styled(Navbar.Toggle)`
  border: none;
`

const BrandLink = styled(Link)`
  display: flex;
  align-items: center;
`

export const Header = () => {
  return (
    <>
      <Navbar collapseOnSelect bg="light" expand="md">
        <BrandLink className="nav-link navbar-brand" to="/">
          <img alt="" src={logo} width="30" height="30" className="d-inline-block align-top mr-3" />
          ChordPic
        </BrandLink>
        <NavbarToggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <span className="mr-auto" />
          <Nav>
            <Link className="nav-link" to="/help">
              Help
            </Link>
            <Link className="nav-link" to="/about">
              About
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}

export default Header
