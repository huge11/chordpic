import * as React from 'react'
import { memo, MutableRefObject, useEffect, useRef } from 'react'
import { ChordSettings, SVGuitarChord } from 'svguitar'
import styled from '@emotion/styled'
import { Chart } from '../interfaces/chart'

interface IProps {
  chart: Chart
  chartRef: MutableRefObject<HTMLDivElement | null>
  onSize: (size: { width: number; height: number }) => void
}

const ChordChartDiv = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: stretch;
  max-height: 40rem;
  padding-top: 2rem;

  svg {
    height: 100%;
    width: 100%;
  }
`

const defaultSVGuitarSettings: Partial<ChordSettings> = {
  fretSize: 1.75,
  barreChordRadius: 0.5
}

export const ChordResult = memo(({ chart, chartRef, onSize }: IProps) => {
  const svguitarRef = useRef<SVGuitarChord>()

  useEffect(() => {
    if (chartRef.current && !svguitarRef.current) {
      svguitarRef.current = new SVGuitarChord(chartRef.current)
    }

    if (svguitarRef.current) {
      const size = svguitarRef.current
        .configure({
          ...defaultSVGuitarSettings,
          ...chart.settings
        })
        .chord(chart.chord)
        .draw()

      onSize(size)
    }
  }, [chart, chartRef, onSize])

  return <ChordChartDiv id="chord-result" ref={chartRef} />
})
