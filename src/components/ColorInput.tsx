import React, { useRef, useState } from 'react'
import { ChromePicker, ColorResult } from 'react-color'
import styled from '@emotion/styled'
import Button from 'react-bootstrap/Button'
import { useOutsideHandler } from '../hooks/use-outside-click'

const ColorPickerContainer = styled.div`
  position: relative;
`

const ColorPreview = styled.span<{ color?: string }>`
  width: 1em;
  height: 1em;
  border: 2px solid #ced4da;
  border-radius: 5px;
  margin-right: 10px;
  display: inline-block;
  background-color: ${props => props.color || '000#'};
`

const ColorPreviewButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`

const ClickWrapper = styled.div`
  position: absolute;
  top: 60px;
  z-index: 10;
`

interface Props {
  value?: string
  onChange: (color: string) => void
}

export const ColorInput = (props: Props) => {
  const [visible, setVisible] = useState(false)
  const ref = useRef<HTMLDivElement | null>(null)
  useOutsideHandler(ref, () => setVisible(false))

  const onColorChange = ({ rgb }: ColorResult) => {
    const rgba = rgb.a ? `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})` : `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`
    props.onChange(rgba)
  }

  return (
    <ColorPickerContainer>
      <ColorPreviewButton variant="outline-dark" onClick={() => setVisible(!visible)}>
        <ColorPreview color={props.value} />
        Select Color...
      </ColorPreviewButton>
      {visible && (
        <ClickWrapper ref={ref}>
          <ChromePicker
            color={props.value}
            onChangeComplete={onColorChange}
            styles={{
              default: {
                picker: {
                  fontFamily: 'Patrick Hand',
                  border: '2px solid #ced4da',
                  borderRadius: '5px',
                  boxShadow: 'none'
                }
              }
            }}
          />
        </ClickWrapper>
      )}
    </ColorPickerContainer>
  )
}
