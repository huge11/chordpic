import * as React from 'react'
import { MutableRefObject, useEffect, useRef, useState, TouchEvent } from 'react'
import styled from '@emotion/styled'
import range from 'lodash.range'
import { IChordInputSettings } from './ChordEditor'
import { CellState, ChordMatrix } from '../services/chord-matrix'

interface IProps {
  settings: IChordInputSettings
  matrix: ChordMatrix
  onMatrixChange: (newMatrix: ChordMatrix) => void
}

const StyledChordInput = styled.div<IChordInputSettings & { numFrets: number; numStrings: number }>`
  display: grid;
  margin-left: ${props => props.width / props.numStrings / 2}px;
  position: relative;
  width: ${props => props.width - props.width / props.numStrings}px;
  grid-template-columns: repeat(${props => props.numStrings - 1}, 1fr);
  grid-template-rows: repeat(${props => props.numFrets}, ${props => props.height / 4}px);
  grid-gap: ${props => props.lineWidth}px;
  padding: ${props => props.lineWidth}px ${props => props.lineWidth}px 0 ${props => props.lineWidth}px;

  background-color: black;

  .cell {
    background-color: var(--white);
  }
`

const ClickCellContainer = styled.div<IChordInputSettings & { numFrets: number; numStrings: number }>(
  props => `
  left: -${props.width / props.numStrings / 2}px;
  position: absolute;
  width: ${props.width}px;
  display: grid;
  grid-template-columns: repeat(${props.numStrings}, 1fr);
  grid-row-gap: ${props.lineWidth}px;
  grid-template-rows: repeat(${props.numFrets}, ${props.height / 4}px);
  padding: ${props.lineWidth}px ${props.lineWidth}px 0 ${props.lineWidth}px;
`
)

interface IClickCellProps {
  circleSize: number
  state: CellState
}

const ClickCell = styled.div<IClickCellProps>(
  ({ circleSize, state }) => `
  
  background-color: transparent;
  background-repeat: no-repeat;
  border: none;
  cursor: pointer;
  overflow: hidden;
  outline: none;
  position: relative;
  cursor: pointer;
  
  ${
    state !== CellState.INACTIVE
      ? `
  ::after {
    content: '';
    height: ${circleSize}px;
    width: ${circleSize}px;
    background-color: black;
    border-radius: ${circleSize / 2}px;
    display: block;
    position: absolute;
    top: calc(50% - ${circleSize / 2}px);
    left: calc(50% - ${circleSize / 2}px);
  }
  `
      : ''
  }
  
  ${
    state === CellState.MIDDLE
      ? `
  ::after {
    border-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
  
  ${
    state === CellState.LEFT
      ? `
  ::after {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
  
  ${
    state === CellState.RIGHT
      ? `
  ::after {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
  
  ${
    state === CellState.MIDDLE_HL
      ? `
  ::after {
    background-color: rgba(0, 0, 0, 0.2);
    border-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
  
  ${
    state === CellState.LEFT_HL
      ? `
  ::after {
    background-color: rgba(0, 0, 0, 0.2);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
  
  ${
    state === CellState.RIGHT_HL
      ? `
  ::after {
    background-color: rgba(0, 0, 0, 0.2);
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    left: 0;
    width: 100%;
  }
  `
      : ''
  }
`
)

/**
 * Hook that alerts clicks outside of the passed ref
 */
function useOutsideAlerter(ref: MutableRefObject<HTMLDivElement | null>, handler: () => void) {
  /**
   * Alert if clicked on outside of element
   */
  function handleMouseUpOutside(event: MouseEvent) {
    if (ref.current && !ref.current.contains(event.target as Node)) {
      handler()
    }
  }

  useEffect(() => {
    // Bind the event listener
    document.addEventListener('mouseup', handleMouseUpOutside)
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mouseup', handleMouseUpOutside)
    }
  })
}

function getTargetStringIndex(e: TouchEvent<HTMLDivElement>): number | null {
  // hack to find the cell from which the finger was lifted
  const stringIndexDataAttr = 'data-string-index'
  const location = e.nativeEvent.changedTouches[0]
  const touchCellDomEl = document.elementFromPoint(location.clientX, location.clientY)
  if (!touchCellDomEl || !touchCellDomEl.hasAttribute(stringIndexDataAttr)) {
    return null
  }

  return Number(touchCellDomEl.getAttribute(stringIndexDataAttr))
}

export default (props: IProps) => {
  const wrapperRef = useRef<HTMLDivElement>(null)
  const [startFrom, setStartFrom] = useState<{ stringIndex: number; fretIndex: number } | null>(null)

  useOutsideAlerter(wrapperRef, () => {
    const newMatrix = matrix.connectHighlighted()
    if (matrix !== newMatrix) {
      onMatrixChange(newMatrix)
      setStartFrom(null)
    }
  })

  const onMatrixChange = (newMatrix: ChordMatrix) => {
    // only call props.onMatrixChange if the matrix actually changed
    if (newMatrix !== matrix) {
      props.onMatrixChange(newMatrix)
    }
  }

  const { matrix } = props

  return (
    <StyledChordInput {...props.settings} ref={wrapperRef} numFrets={matrix.numFrets} numStrings={matrix.numStrings}>
      {range(matrix.numStrings * matrix.numFrets - 1).map(i => (
        <div key={i} className="cell" data-cell-index={i} />
      ))}

      <ClickCellContainer {...props.settings} numFrets={matrix.numFrets} numStrings={matrix.numStrings}>
        {matrix.rows.map((row, fretIndex) =>
          row.map((state, stringIndex) => (
            <ClickCell
              circleSize={props.settings.circleSize}
              state={state}
              data-string-index={stringIndex}
              data-row-index={fretIndex}
              key={`${fretIndex}-${stringIndex}`}
              onClick={() => onMatrixChange(matrix.toggle(stringIndex, fretIndex))}
              onMouseDown={() => setStartFrom({ stringIndex, fretIndex })}
              onMouseEnter={() => {
                if (startFrom && Math.abs(stringIndex - startFrom.stringIndex) > 0) {
                  onMatrixChange(matrix.connectHighlight(startFrom.fretIndex, startFrom.stringIndex, stringIndex))
                }
              }}
              onTouchStart={() => setStartFrom({ stringIndex, fretIndex })}
              onTouchMove={e => {
                const targetStringIndex = getTargetStringIndex(e)
                if (targetStringIndex === null) {
                  return
                }

                if (startFrom && Math.abs(targetStringIndex - startFrom.stringIndex) > 0) {
                  onMatrixChange(matrix.connectHighlight(startFrom.fretIndex, startFrom.stringIndex, targetStringIndex))
                }
              }}
              onMouseUp={() => {
                onMatrixChange(matrix.connectHighlighted())
                setStartFrom(null)
              }}
              onTouchEnd={() => {
                onMatrixChange(matrix.connectHighlighted())
                setStartFrom(null)
              }}
            />
          ))
        )}
      </ClickCellContainer>
    </StyledChordInput>
  )
}
