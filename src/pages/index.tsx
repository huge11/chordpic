import * as React from 'react'
import { useRef, useState } from 'react'
import IndexLayout from '../layouts'
import { ChordEditor } from '../components/ChordEditor'
import { ChordResult } from '../components/ChordResult'

import DownloadButtons from '../components/DownloadButtons'
import { Frame } from '../components/Frame'
import { Chart } from '../interfaces/chart'
import '../styles/index.scss'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { ChordForm } from '../components/ChordForm'
import styled from '@emotion/styled'
import ShareButtons from '../components/ShareButtons'
import { useResizeHandler } from '../hooks/use-resize-handler'
import { usePersistedState } from '../hooks/persisted-state'
import { ChordStyle } from 'svguitar'

const initialSettings = Object.freeze({
  style: ChordStyle.normal,
  strings: 6,
  frets: 5,
  position: 1,
  nutSize: 0.65,
  titleFontSize: 48,
  color: '#000',
  strokeWidth: 2,
  fretSize: 1.5,
  backgroundColor: 'none',
  fixedDiagramPosition: false
})

const ColCenterContent = styled(Col)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
`

const IndexPage = () => {
  const chartRef = useRef<HTMLDivElement | null>(null)

  const [chart, setChart] = usePersistedState<Chart>('chart_v1', {
    chord: {
      fingers: [],
      barres: []
    },
    settings: initialSettings
  })
  const [size, setSize] = useState<{ width: number; height: number }>({ width: 0, height: 0 })
  const { width, height } = useResizeHandler()

  const resetSettings = () => setChart({ ...chart, settings: initialSettings })

  const {
    settings: { frets, strings }
  } = chart

  return (
    <IndexLayout>
      <ChordForm onResetSettings={resetSettings} settings={chart.settings} onSettings={settings => setChart({ ...chart, settings })} />
      <Row className="my-5">
        <ColCenterContent md={6}>
          <Frame text="Editor">
            <ChordEditor
              width={width * 0.75}
              height={height * 0.6}
              numFrets={frets || initialSettings.frets}
              numStrings={strings || initialSettings.strings}
              settings={chart.settings}
              chord={chart.chord}
              onChart={chart => setChart(chart)}
            />
          </Frame>
        </ColCenterContent>
        <ColCenterContent md={6}>
          <Frame text="Result" stretch={true}>
            <ChordResult chart={chart} chartRef={chartRef} onSize={setSize} />
          </Frame>
        </ColCenterContent>
      </Row>
      <Row>
        <Col md={6}>
          <DownloadButtons chartRef={chartRef} size={size} />
        </Col>
        <Col md={6}>
          <ShareButtons chart={chart} />
        </Col>
      </Row>
    </IndexLayout>
  )
}

export default IndexPage
