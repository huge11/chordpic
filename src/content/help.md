---
layout: page
title: 'Help'
---

If you haven't figured it out already, here's how you create a chord chart and then save it as a PNG or SVG image. Don't worry, it's super simple!

ChordPic has 3 main sections: The _Editor_, the _Result_ and the _Download and Share_ section.

As the name suggests, in the editor section you edit you chord chart. Every change of the chord chart is done in this section! More on this section later.

In the Result section you can see a preview of your chord chart.

The download and share section allows you to download your chart in different formats and sharing the charts on different platforms.

## The Editor

**Adding / removing fingers**: Simply click anywhere you want the finger to appear. To remove the finger, just click on it again and it will disappear.

![Example of adding and removing fingers](images/toggle.gif)

**Toggling silent or open strings**: If there is no finger on a string, an 'O' automatically appears above the string (open string). If you want to change that to an 'X' (don't play that string) simply click on the 'O' to make it an 'X'. When you click it again it will change back to an 'O'.

![Example of toggling strings from do not play to open](images/silentstrings.gif)

**Adding a barre chord**: To add a barre chord, you can simply connect the strings with the mouse or if you're on mobile you can swipe from one string to another with your finger. To remove the bare chord simply click anywhere on the fret with the barre chord to remove it.

![Example of adding and removing a barre chord](images/barre.gif)

**Adding labels to strings**: To label the strings you can enter any letters or numbers below the strings. By default the strings are not labelled.

![Example of adding and removing a barre chord](images/labels.gif)

**Adding labels to strings**: To label the strings you can enter any letters or numbers below the strings. By default the strings are not labelled.

## The Result Section

The result section gives you a preview of what your chart image will look like. All changes made in the editor section are immediately visible in the result section. Sample chart:

![Example chord chart](images/samplechord.png)

## The Download & Sharing Section

In the download section you can download your chord chart as an image. You can export the image as an SVG or PNG image. When you download a PNG image, you have to chose between different resolutions. The height of the images can vary and depend on what your chart looks like.
The width will always stay the same though, no matter what your chart looks like.

In the share section you can generate a link that you can share with other people. All your settings and the whole chart
are saved _in that link_. The sharing section also allows you to share your charts on many different platforms or messengers.
